// parallax.frag
#version 330

// Some drivers require the following
//precision highp float;

struct lightStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 position;
	float radius;
};

struct materialStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 emissive;
	float shininess;
};
//uniforms sent at render time
uniform lightStruct light;
uniform materialStruct material;
uniform vec2 scaleBias;

//texture information
uniform sampler2D colourMap;
uniform sampler2D normalMap;
uniform sampler2D specularMap;
uniform sampler2D heightMap;
uniform sampler2D glossMap;

//inputs from vertex shader stage
in VertexData
{
	vec3 ts_L; //view vector tangent space
	vec3 ts_V; //light vector tangent space
	vec3 ws_L; //world space light vector
	vec3 ws_V; //world space view vector
	vec3 ws_N; //world space surface normal
	vec2 ex_TexCoord;
	mat3 tbn;
	float dist;
}	vertex;

//final fragment colour
layout(location = 0) out vec4 out_Color;

float parallaxSoftShadowMultiplier(in vec3 L, in vec2 initialTexCoord,
                                       in float initialHeight)
{
   float shadowMultiplier = 1;

   const float minLayers = 15;
   const float maxLayers = 30;

   // calculate lighting only for surface oriented to the light source
   if(dot(vec3(0, 0, 1), L) > 0)
   {
      // calculate initial parameters
      float numSamplesUnderSurface	= 0;
      shadowMultiplier	= 0;
      float numLayers	= mix(maxLayers, minLayers, abs(dot(vec3(0, 0, 1), L)));
      float layerHeight	= initialHeight / numLayers;
      vec2 texStep	= scaleBias.r * L.xy / L.z / numLayers;

      // current parameters
      float currentLayerHeight	= initialHeight - layerHeight;
      vec2 currentTextureCoords	= initialTexCoord + texStep;
      float heightFromTexture	= texture(heightMap, currentTextureCoords).r;
      int stepIndex	= 1;

      // while point is below depth 0.0 )
      while(currentLayerHeight > 0)
      {
         // if point is under the surface
         if(heightFromTexture < currentLayerHeight)
         {
            // calculate partial shadowing factor
            numSamplesUnderSurface	+= 1;
            float newShadowMultiplier	= (currentLayerHeight - heightFromTexture) *
                                             (1.0 - stepIndex / numLayers);
            shadowMultiplier	= max(shadowMultiplier, newShadowMultiplier);
         }

         // offset to the next layer
         stepIndex	+= 1;
         currentLayerHeight	-= layerHeight;
         currentTextureCoords	+= texStep;
         heightFromTexture	= texture(heightMap, currentTextureCoords).r;
      }

      // Shadowing factor should be 1 if there were no points under the surface
      if(numSamplesUnderSurface < 1)
      {
         shadowMultiplier = 1;
      }
      else
      {
         shadowMultiplier = 1.0 - shadowMultiplier;
      }
   }
   return shadowMultiplier;
}


//parallax mapping function
vec2 parallaxMap(vec3 view, vec2 textureCoords, out float height)
{
	// get depth for this fragment
   float initialHeight = texture(heightMap, textureCoords).r;

   // calculate amount of offset for Parallax Mapping
   vec2 texCoordOffset = scaleBias.r * view.xy / view.z * initialHeight;

   // calculate amount of offset for Parallax Mapping With Offset Limiting
   texCoordOffset = scaleBias.r * view.xy * initialHeight;

   // retunr modified texture coordinates
   return textureCoords - texCoordOffset;
}

//////////////////////////////////////////////////////
// Calculates lighting by Blinn-Phong model and Normal Mapping
// Returns color of the fragment
vec4 normalMappingLighting(in vec2 T, in vec3 L, in vec3 V, float shadowMultiplier)
{
   // restore normal from normal map
	vec3 N = vertex.tbn*normalize(texture(normalMap, T).xyz * 2.0 - 1.0);
	vec3 D = texture(colourMap, T).rgb;
	vec3 resColor = vec3(0.0);

	// ambient lighting
	float iamb = 0.2;
	// first, check if the normal and light directions are not perpendicular or larger
	float NdotL = dot(vertex.ws_N, L);
	if(NdotL > 0)
	{
		 vec3 idiff = clamp(dot(N, L), 0, 1) * light.diffuse.rgb * D;
		 // specular lighting
		 vec3 ispec;
		 vec3 R = reflect(-L, N);
		 float RdotV = dot(R,V);
		 RdotV = clamp( RdotV, 0, 1);
		 float gloss = texture(glossMap,T).r;
		 ispec = pow( max( RdotV, 0),gloss) * texture(specularMap, T).rgb * light.specular.rgb;

		 resColor.rgb = (D * (light.ambient.xyz + (idiff + ispec) * pow(shadowMultiplier, 4))) * (1.0/(1.0 + 0.1*vertex.dist + 0.02 *(vertex.dist * vertex.dist)));
	}
	return vec4(resColor,1.0);
	//return vec4(N,1.0);
}



void main(void) {
   // normalize vectors after vertex shader
   vec3 V = normalize(vertex.ts_V);
   vec3 L = normalize(vertex.ts_L);

   // get new texture coordinates from Parallax Mapping
   float parallaxHeight;
   vec2 T = parallaxMap(V, vertex.ex_TexCoord, parallaxHeight);
   vec3 H = normalize(vertex.ws_V+vertex.ws_L);
   // get self-shadowing factor for elements of parallax
   float shadowMultiplier = parallaxSoftShadowMultiplier(L, T, parallaxHeight - 0.05);

   // calculate lighting
   out_Color = normalMappingLighting(T, H, vertex.ws_V, shadowMultiplier);
}