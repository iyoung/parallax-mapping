// Based loosly on the first triangle OpenGL tutorial
// http://www.opengl.org/wiki/Tutorial:_OpenGL_3.1_The_First_Triangle_%28C%2B%2B/Win%29
// This program will render two triangles
// Most of the OpenGL code for dealing with buffer objects, etc has been moved to a 
// utility library, to make creation and display of mesh objects as simple as possible

// Windows specific: Uncomment the following line to open a console window for debug output
#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

#include "rt3d.h"
#include <glm\glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <stack>
#include "OBJFileLoader.h"
#include "rt3dObjLoader.h"
#include <ctime>
#include <SDL.h>
#include <SDL_ttf.h>
using namespace std;

// Globals
// Real programs don't use globals :-D
// Data would normally be read from files
std::stack<glm::mat4> mvStack;
GLfloat rotY,rotX;
GLfloat f;
GLfloat rot=50.0f;
GLfloat savedRot;
float angle;
bool paused;
int xpos, ypos;
// position
glm::vec3 position = glm::vec3( 0, 0,0 );
// horizontal angle : toward -Z
float horizontalAngle = 3.14f;
// vertical angle : 0, look at the horizon
float verticalAngle = 0.0f;
// roll angle
float rollAngle=0.0f;
// Initial Field of View
float initialFoV = 45.0f;
glm::vec2 mouse;
float speed = 1.5f; // 3 units / second
float mouseSpeed = 0.4f;
float scrWidth = 1366.0f;
float scrHeight = 768.0f;
glm::vec2 scaleBias;

rt3d::lightStruct light0 = {
	{0.2f, 0.2f, 0.2f, 1.0f}, // ambient
	{1.0f, 1.0f, 1.0f, 1.0f}, // diffuse
	{1.0f, 1.0f, 1.0f, 1.0f}, // specular
	{0.0f, 0.0f, 0.0f, 0.0f},  // position
	2.0f						//radius
};
glm::vec4 lightPos(-5.0f, 2.0f, 2.0f, 1.0f); //light position

rt3d::materialStruct material0 = {
	{0.2f, 0.2f, 0.2f, 1.0f },	// ambient
	{0.4f, 0.4f, 0.4f, 1.0f },	// diffuse
	{1.0f, 0.5f, 0.5f, 1.0f },	// specular
	{0.0f, 0.0f, 0.0f, 0.0f	},	//emissive
	1.0f  // shininess

};
rt3d::materialStruct material1 = {
	{0.5f,0.5f,0.5f,1.0f},		//ambient
	{1.0f,1.0f,1.0f,1.0f},		//diffuse
	{1.0f,1.0f,1.0f,1.0f},		//specular
	{0.0f,0.0f,0.0f,0.0f},		// emissive
	10.0f						//shininess
};
rt3d::materialStruct material2 = {
	{0.2f, 0.2f, 0.4f, 1.0f},	// ambient
	{0.5f, 0.5f, 0.8f, 1.0f},	// diffuse
	{0.8f, 0.8f, 1.0f, 1.0f},	// specular
	{1.0f, 1.0f, 1.0f, 1.0f},	// emissive
	5.0f  // shininess

};
TTF_Font * textFont;
int modeSelector;
GLuint planeVertCount;
GLuint sphereVertCount;
clock_t timer;
clock_t now;
clock_t deltaTime;
bool clockwise;
std::vector<GLfloat> cubeVerts;
std::vector<GLfloat> cubeNormals;
std::vector<GLuint> cubeIndices;
std::vector<GLfloat> cubeTangents;
glm::vec3 rightD;
glm::vec3 directionD;
GLuint textures[11];
std::vector<GLfloat> texCoords;
glm::vec3 up;
GLuint shaderProgram;
GLuint basicShader;
int mouseX = 0;
int mouseY = 0;
int LastX;
int LastY;
GLuint meshObjects[5];
float dx,dy,dz;
glm::mat4 viewMatrix;
bool keybuffer[256];
bool keyHeldBuffer[256];

// Set up rendering context
enum meshes
{
	WALL,
	FLOOR,
	LIGHT
};

GLuint textToTexture(const char * str/*, TTF_Font *font, SDL_Color colour, GLuint &w,GLuint &h */) {
	TTF_Font *font = textFont;
	SDL_Color colour = { 255, 255, 255 };
	SDL_Color bg = { 0, 0, 0 };

	SDL_Surface *stringImage;
	stringImage = TTF_RenderText_Blended(font,str,colour);

	if (stringImage == NULL)
		//exitFatalError("String surface not created.");
		std::cout << "String surface not created." << std::endl;

	GLuint w = stringImage->w;
	GLuint h = stringImage->h;
	GLuint colours = stringImage->format->BytesPerPixel;

	GLuint format, internalFormat;
	if (colours == 4) {   // alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGBA;
	    else
		    format = GL_BGRA;
	} else {             // no alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGB;
	    else
		    format = GL_BGR;
	}
	internalFormat = (colours == 4) ? GL_RGBA : GL_RGB;

	GLuint texture;

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, w, h, 0,
                    format, GL_UNSIGNED_BYTE, stringImage->pixels);

	// SDL surface was used to generate the texture but is no longer
	// required. Release it to free memory
	SDL_FreeSurface(stringImage);
	return texture;
}
SDL_Window * setupRC(SDL_GLContext &context) {
	SDL_Window * window;
    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        rt3d::exitFatalError("Unable to initialize SDL"); 
	  
    // Request an OpenGL 3.0 context.
    // Not able to use SDL to choose profile (yet), should default to core profile on 3.2 or later
	// If you request a context not supported by your drivers, no OpenGL context will be created

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE); 

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); // Turn on x4 multisampling anti-aliasing (MSAA)
 
    // Create 800x600 window
    window = SDL_CreateWindow("SDL/GLM/OpenGL Demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        scrWidth, scrHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!window) // Check window was created OK
        rt3d::exitFatalError("Unable to create window");
 
    context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		//SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
	return window;
}


void init(void) {
		// set up TrueType / SDL_ttf
	if (TTF_Init()== -1)
		cout << "TTF failed to initialise." << endl;

	textFont = TTF_OpenFont("MavenPro-Regular.ttf", 24);
	if (textFont == NULL)
		cout << "Failed to open font." << endl;
	// For this simple example we'll be using the most basic of shader programs
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	timer = clock();
	now = timer;
	//load model assets
	rt3d::loadObj("plane.obj",cubeVerts,cubeNormals,texCoords,cubeIndices);
	rt3d::calculateTangents(cubeTangents, cubeVerts, cubeNormals, texCoords, cubeIndices);
	meshObjects[WALL] = rt3d::createMeshWithTans(cubeIndices.size(),cubeVerts.data(),cubeTangents.data(),cubeNormals.data(),texCoords.data(),cubeIndices.size(),cubeIndices.data());
	meshObjects[FLOOR] = meshObjects[WALL];
	planeVertCount = cubeIndices.size();
	cubeVerts.clear();
	cubeIndices.clear();
	cubeNormals.clear();
	cubeTangents.clear();
	texCoords.clear();
	rt3d::loadObj("ball1.obj",cubeVerts,cubeNormals,texCoords,cubeIndices);
	meshObjects[LIGHT] = rt3d::createMesh(cubeIndices.size(),cubeVerts.data(),nullptr,cubeNormals.data(),texCoords.data(),cubeIndices.size(),cubeIndices.data());
	sphereVertCount = cubeIndices.size();
	cubeVerts.clear();
	cubeIndices.clear();
	cubeNormals.clear();
	cubeTangents.clear();
	texCoords.clear();


	clockwise = false;
	//load up our shaders and textures
	shaderProgram = rt3d::initShaders("phong-tex-occlusion.vert","phong-tex-occlusion.frag");
	basicShader = rt3d::initShaders("phong-tex-basic.vert","phong-tex-basic.frag");

	textures[0] = rt3d::loadBitmap("brick2_diffuse.bmp");
	textures[1] = rt3d::loadBitmap("brick2_normal.bmp");
	textures[2] = rt3d::loadBitmap("brick2_diffuse.bmp");
	textures[3] = rt3d::loadBitmap("brick2_height.bmp");
	textures[4] = rt3d::loadBitmap("brick2_gloss.bmp");
	//textures for floor
	textures[5] = rt3d::loadBitmap("Stone_JFdwarvenFloor_512_d.bmp");
	textures[6] = rt3d::loadBitmap("Stone_JFdwarvenFloor_512_n.bmp");
	textures[7] = rt3d::loadBitmap("Stone_JFdwarvenFloor_512_s.bmp");
	textures[8] = rt3d::loadBitmap("Stone_JFdwarvenFloor_512_h.bmp");
	textures[9] = rt3d::loadBitmap("Stone_JFdwarvenFloor_512_g.bmp");
	//white texture for light
	textures[10] = rt3d::loadBitmap("white.bmp");
	f = 60.0f;
	angle = 0.0f;
	// Going to create our mesh objects here
	position = glm::vec3(0.0, 1.0, 0.0f);
	scaleBias = glm::vec2(0.01f,-0.02f);
	lightPos = glm::vec4(0.0f,1.0f,0.0f,1.0f);
	LastX=mouseX;
	LastY=mouseY;
	SDL_SetRelativeMouseMode(SDL_TRUE);

}
bool handleEvents(SDL_Event &event) 
{
	if(event.type == SDL_KEYDOWN)
	{
		if(event.key.keysym.sym == SDLK_ESCAPE) return false;
		keybuffer[event.key.keysym.scancode] = true;
		keyHeldBuffer[event.key.keysym.scancode] = true;
	}
	else if (event.type == SDL_KEYUP)
	{
		keybuffer[event.key.keysym.scancode] = false;
	}
	if(event.type == SDL_MOUSEMOTION)
	{
		if(event.motion.xrel > 0) horizontalAngle -= mouseSpeed*deltaTime/1000.0f;
		if(event.motion.xrel < 0) horizontalAngle += mouseSpeed*deltaTime/1000.0f;
		if(event.motion.yrel > 0) verticalAngle -= mouseSpeed*deltaTime/1000.0f;
		if(event.motion.yrel < 0) verticalAngle += mouseSpeed*deltaTime/1000.0f;
	}

	return true;
}


void updateModel()
{
	now = clock();
	deltaTime = now-timer;
	float turnspeed = 0.005f;
	if (angle<-45)
	{
		clockwise=false;
	}
	if(angle>45)
	{
		clockwise=true;
	}
	if(clockwise)
	{
		angle+=(deltaTime*-turnspeed);
	}
	else
	{
		angle+=(deltaTime*turnspeed);

	}
	const Uint8 *keys = SDL_GetKeyboardState(NULL);



	//calculate direction (forward orientation) and right ascension

	directionD = glm::vec3(	cos(verticalAngle) * sin(horizontalAngle),
							sin(verticalAngle),
							cos(verticalAngle) * cos(horizontalAngle));
	// Right vector
	rightD = glm::vec3(	sin(horizontalAngle - 3.14f/2.0f),
						0,
						cos(horizontalAngle - 3.14f/2.0f));
	
	up = glm::cross( rightD, directionD );

	if ( keys[SDL_SCANCODE_1] ) f-=0.1f;
	if ( keys[SDL_SCANCODE_2] ) f+=0.1f;

	if ( keys[SDL_SCANCODE_P] ) scaleBias.r+=0.0001f;
	if ( keys[SDL_SCANCODE_O] ) scaleBias.r-=0.0001f;
	if ( keys[SDL_SCANCODE_K] ) scaleBias.g+=0.001f;
	if ( keys[SDL_SCANCODE_L] ) scaleBias.g-=0.001f;



	if ( keybuffer[SDL_SCANCODE_KP_2] ) lightPos.z+=speed*(((float)deltaTime)/1000);
	if ( keybuffer[SDL_SCANCODE_KP_8] ) lightPos.z-=speed*(((float)deltaTime)/1000);
	if ( keybuffer[SDL_SCANCODE_KP_6] ) lightPos.x+=speed*(((float)deltaTime)/1000);
	if ( keybuffer[SDL_SCANCODE_KP_4] ) lightPos.x-=speed*(((float)deltaTime)/1000);
	if ( keybuffer[SDL_SCANCODE_KP_MULTIPLY] ) lightPos.y+=speed*(((float)deltaTime)/1000);
	if ( keybuffer[SDL_SCANCODE_KP_DIVIDE] ) lightPos.y-=speed*(((float)deltaTime)/1000);
	if ( keybuffer[SDL_SCANCODE_KP_PLUS]) light0.radius[0] += speed*(((float)deltaTime)/1000);
	if ( keybuffer[SDL_SCANCODE_KP_MINUS]) light0.radius[0] -= speed*(((float)deltaTime)/1000);


	if ( keybuffer[SDL_SCANCODE_A] ) position -= rightD * speed*(((float)deltaTime)/1000);
	if ( keybuffer[SDL_SCANCODE_D]) position += rightD * speed*(((float)deltaTime)/1000);
	if ( keybuffer[SDL_SCANCODE_W]) position += directionD * speed*(((float)deltaTime)/1000);
	if ( keybuffer[SDL_SCANCODE_S])	position -= directionD * speed*(((float)deltaTime)/1000);
	if ( keybuffer[SDL_SCANCODE_SPACE]) position += up * speed* (((float)deltaTime)/1000);

}

void drawWalls()
{
	glm::mat4 projection = glm::perspective(f,scrWidth/scrHeight,0.05f,120.0f);
		viewMatrix       = glm::lookAt(
		position,				// Camera is here
		position+directionD,	// and looks here : at the same position, plus "direction"
		up						// Head is up (set to 0,-1,0 to look upside-down)
		);

		glUseProgram(shaderProgram);
		//draw calls go here
		std::stack<glm::mat4> matStack;
		//set & bind textures
		glActiveTexture(GL_TEXTURE0);
		GLint diffLoc = glGetUniformLocation(shaderProgram,"colourMap");
		glUniform1i(diffLoc,0);
		glBindTexture(GL_TEXTURE_2D,textures[0]);

		glActiveTexture(GL_TEXTURE1);
		GLint normLoc = glGetUniformLocation(shaderProgram,"normalMap");
		glUniform1i(normLoc,1);
		glBindTexture(GL_TEXTURE_2D,textures[1]);

		glActiveTexture(GL_TEXTURE2);
		GLint specLoc = glGetUniformLocation(shaderProgram,"specularMap");
		glUniform1i(specLoc,2);
		glBindTexture(GL_TEXTURE_2D,textures[2]);

		glActiveTexture(GL_TEXTURE3);
		GLint glossLoc = glGetUniformLocation(shaderProgram,"heightMap");
		glUniform1i(glossLoc,3);
		glBindTexture(GL_TEXTURE_2D,textures[3]);

		glActiveTexture(GL_TEXTURE4);
		GLint heightLoc = glGetUniformLocation(shaderProgram,"glossMap");
		glUniform1i(heightLoc,4);
		glBindTexture(GL_TEXTURE_2D,textures[4]);


		matStack.push(glm::mat4(1.0));
		matStack.top() = glm::translate(matStack.top(),glm::vec3(0.0f,1.0f,-2.0f));
		matStack.top() = glm::rotate(matStack.top(),90.0f,glm::vec3(1.0f,0.0f,0.0f));
		glm::mat4 mv = viewMatrix * matStack.top();
		//set uniform
		rt3d::setLight(shaderProgram,light0);

		rt3d::setUniformMatrix4fv(shaderProgram,"projection",glm::value_ptr(projection));
		rt3d::setUniformMatrix4fv(shaderProgram,"view",glm::value_ptr(viewMatrix));
		rt3d::setUniformMatrix4fv(shaderProgram,"model",glm::value_ptr(matStack.top()));
		rt3d::setUniformMatrix3fv(shaderProgram, "normalMat", glm::value_ptr(glm::transpose(glm::inverse(glm::mat3(mv)))));
		rt3d::setLightPos(shaderProgram,glm::value_ptr(lightPos));

		GLuint uniform = glGetUniformLocation(shaderProgram,"eyePosition");
		glUniform3fv(uniform,1,glm::value_ptr(position));
		uniform = glGetUniformLocation(shaderProgram, "scaleBias");
		glUniform2fv(uniform,1,glm::value_ptr(scaleBias));
		rt3d::setEmissiveMaterial(shaderProgram,material1);

		rt3d::drawIndexedMesh(meshObjects[WALL],planeVertCount,GL_TRIANGLES);
		matStack.pop();
		matStack.push(glm::mat4(1.0));
		matStack.top() = glm::translate(matStack.top(),glm::vec3(2.0f,1.0f,-2.0f));
		matStack.top() = glm::rotate(matStack.top(),90.0f,glm::vec3(1.0f,0.0f,0.0f));
		mv = viewMatrix * matStack.top();
		//set uniform
		rt3d::setLight(shaderProgram,light0);

		rt3d::setUniformMatrix4fv(shaderProgram,"projection",glm::value_ptr(projection));
		rt3d::setUniformMatrix4fv(shaderProgram,"view",glm::value_ptr(viewMatrix));
		rt3d::setUniformMatrix4fv(shaderProgram,"model",glm::value_ptr(matStack.top()));
		rt3d::setUniformMatrix3fv(shaderProgram, "normalMat", glm::value_ptr(glm::transpose(glm::inverse(glm::mat3(mv)))));
		rt3d::setLightPos(shaderProgram,glm::value_ptr(lightPos));


		uniform = glGetUniformLocation(shaderProgram,"eyePosition");
		glUniform3fv(uniform,1,glm::value_ptr(position));
		uniform = glGetUniformLocation(shaderProgram, "scaleBias");
		glUniform2fv(uniform,1,glm::value_ptr(scaleBias));
		
		rt3d::setEmissiveMaterial(shaderProgram,material1);

		rt3d::drawIndexedMesh(meshObjects[WALL],planeVertCount,GL_TRIANGLES);
		matStack.pop();

		matStack.push(glm::mat4(1.0));
		matStack.top() = glm::translate(matStack.top(),glm::vec3(-2.0f,1.0f,-2.0f));
		matStack.top() = glm::rotate(matStack.top(),90.0f,glm::vec3(1.0f,0.0f,0.0f));
		mv = viewMatrix * matStack.top();
		//set uniform
		rt3d::setLight(shaderProgram,light0);

		rt3d::setUniformMatrix4fv(shaderProgram,"projection",glm::value_ptr(projection));
		rt3d::setUniformMatrix4fv(shaderProgram,"view",glm::value_ptr(viewMatrix));
		rt3d::setUniformMatrix4fv(shaderProgram,"model",glm::value_ptr(matStack.top()));
		rt3d::setUniformMatrix3fv(shaderProgram, "normalMat", glm::value_ptr(glm::transpose(glm::inverse(glm::mat3(mv)))));
		rt3d::setLightPos(shaderProgram,glm::value_ptr(lightPos));

		uniform = glGetUniformLocation(shaderProgram,"eyePosition");
		glUniform3fv(uniform,1,glm::value_ptr(position));
		uniform = glGetUniformLocation(shaderProgram, "scaleBias");
		glUniform2fv(uniform,1,glm::value_ptr(scaleBias));
		
		rt3d::setEmissiveMaterial(shaderProgram,material1);

		rt3d::drawIndexedMesh(meshObjects[WALL],planeVertCount,GL_TRIANGLES);
		matStack.pop();


		matStack.push(glm::mat4(1.0));
		matStack.top() = glm::translate(matStack.top(),glm::vec3(0.0f,1.0f,2.0f));

		matStack.top() = glm::rotate(matStack.top(), 270.0f,glm::vec3(1.0f,0.0f,0.0f));
		matStack.top() = glm::rotate(matStack.top(), 180.0f, glm::vec3(0.0f, 1.0f, 0.0f));

		mv = viewMatrix * matStack.top();
		//set uniform
		rt3d::setLight(shaderProgram,light0);

		rt3d::setUniformMatrix4fv(shaderProgram,"projection",glm::value_ptr(projection));
		rt3d::setUniformMatrix4fv(shaderProgram,"view",glm::value_ptr(viewMatrix));
		rt3d::setUniformMatrix4fv(shaderProgram,"model",glm::value_ptr(matStack.top()));
		rt3d::setUniformMatrix3fv(shaderProgram, "normalMat", glm::value_ptr(glm::transpose(glm::inverse(glm::mat3(mv)))));
		rt3d::setLightPos(shaderProgram,glm::value_ptr(lightPos));

		uniform = glGetUniformLocation(shaderProgram,"eyePosition");
		glUniform3fv(uniform,1,glm::value_ptr(position));
		uniform = glGetUniformLocation(shaderProgram, "scaleBias");
		glUniform2fv(uniform,1,glm::value_ptr(scaleBias));
		
		rt3d::setEmissiveMaterial(shaderProgram,material1);

		rt3d::drawIndexedMesh(meshObjects[WALL],planeVertCount,GL_TRIANGLES);
		matStack.pop();


		matStack.push(glm::mat4(1.0));
		matStack.top() = glm::translate(matStack.top(),glm::vec3(2.0f,1.0f,2.0f));

		matStack.top() = glm::rotate(matStack.top(),-90.0f,glm::vec3(1.0f,0.0f,0.0f));
		matStack.top() = glm::rotate(matStack.top(), 180.0f, glm::vec3(0.0f, 1.0f, 0.0f));

		mv = viewMatrix * matStack.top();
		//set uniform
		rt3d::setLight(shaderProgram,light0);

		rt3d::setUniformMatrix4fv(shaderProgram,"projection",glm::value_ptr(projection));
		rt3d::setUniformMatrix4fv(shaderProgram,"view",glm::value_ptr(viewMatrix));
		rt3d::setUniformMatrix4fv(shaderProgram,"model",glm::value_ptr(matStack.top()));
		rt3d::setUniformMatrix3fv(shaderProgram, "normalMat", glm::value_ptr(glm::transpose(glm::inverse(glm::mat3(mv)))));
		rt3d::setLightPos(shaderProgram,glm::value_ptr(lightPos));

		uniform = glGetUniformLocation(shaderProgram,"eyePosition");
		glUniform3fv(uniform,1,glm::value_ptr(position));
		uniform = glGetUniformLocation(shaderProgram, "scaleBias");
		glUniform2fv(uniform,1,glm::value_ptr(scaleBias));
		
		rt3d::setEmissiveMaterial(shaderProgram,material1);

		rt3d::drawIndexedMesh(meshObjects[WALL],planeVertCount,GL_TRIANGLES);
		matStack.pop();

		matStack.push(glm::mat4(1.0));
		matStack.top() = glm::translate(matStack.top(),glm::vec3(-2.0f,1.0f,2.0f));

		matStack.top() = glm::rotate(matStack.top(),-90.0f,glm::vec3(1.0f,0.0f,0.0f));
		matStack.top() = glm::rotate(matStack.top(), 180.0f, glm::vec3(0.0f, 1.0f, 0.0f));

		mv = viewMatrix * matStack.top();
		//set uniform
		rt3d::setLight(shaderProgram,light0);

		rt3d::setUniformMatrix4fv(shaderProgram,"projection",glm::value_ptr(projection));
		rt3d::setUniformMatrix4fv(shaderProgram,"view",glm::value_ptr(viewMatrix));
		rt3d::setUniformMatrix4fv(shaderProgram,"model",glm::value_ptr(matStack.top()));
		rt3d::setUniformMatrix3fv(shaderProgram, "normalMat", glm::value_ptr(glm::transpose(glm::inverse(glm::mat3(mv)))));
		rt3d::setLightPos(shaderProgram,glm::value_ptr(lightPos));

		uniform = glGetUniformLocation(shaderProgram,"eyePosition");
		glUniform3fv(uniform,1,glm::value_ptr(position));
		uniform = glGetUniformLocation(shaderProgram, "scaleBias");
		glUniform2fv(uniform,1,glm::value_ptr(scaleBias));
		
		rt3d::setEmissiveMaterial(shaderProgram,material1);

		rt3d::drawIndexedMesh(meshObjects[WALL],planeVertCount,GL_TRIANGLES);
		matStack.pop();
}
void drawFloors()
{
		glm::mat4 projection = glm::perspective(f,scrWidth/scrHeight,0.05f,120.0f);
		viewMatrix       = glm::lookAt(
		position,				// Camera is here
		position+directionD,	// and looks here : at the same position, plus "direction"
		up						// Head is up (set to 0,-1,0 to look upside-down)
		);

		glUseProgram(shaderProgram);
		//draw calls go here
		std::stack<glm::mat4> matStack;
		//set & bind textures
		glActiveTexture(GL_TEXTURE0);
		GLint diffLoc = glGetUniformLocation(shaderProgram,"colourMap");
		glUniform1i(diffLoc,0);
		glBindTexture(GL_TEXTURE_2D,textures[5]);

		glActiveTexture(GL_TEXTURE1);
		GLint normLoc = glGetUniformLocation(shaderProgram,"normalMap");
		glUniform1i(normLoc,1);
		glBindTexture(GL_TEXTURE_2D,textures[6]);

		glActiveTexture(GL_TEXTURE2);
		GLint specLoc = glGetUniformLocation(shaderProgram,"specularMap");
		glUniform1i(specLoc,2);
		glBindTexture(GL_TEXTURE_2D,textures[7]);

		glActiveTexture(GL_TEXTURE3);
		GLint glossLoc = glGetUniformLocation(shaderProgram,"heightMap");
		glUniform1i(glossLoc,3);
		glBindTexture(GL_TEXTURE_2D,textures[8]);

		glActiveTexture(GL_TEXTURE4);
		GLint heightLoc = glGetUniformLocation(shaderProgram,"glossMap");
		glUniform1i(heightLoc,4);
		glBindTexture(GL_TEXTURE_2D,textures[9]);

		matStack.push(glm::mat4(1.0));
		matStack.top() = glm::translate(matStack.top(),glm::vec3(0.0f,0.0f,1.0f));
		
		glm::mat4 mv = viewMatrix * matStack.top();
		//set uniform
		rt3d::setLight(shaderProgram,light0);

		rt3d::setUniformMatrix4fv(shaderProgram,"projection",glm::value_ptr(projection));
		rt3d::setUniformMatrix4fv(shaderProgram,"view",glm::value_ptr(viewMatrix));
		rt3d::setUniformMatrix4fv(shaderProgram,"model",glm::value_ptr(matStack.top()));
		rt3d::setUniformMatrix3fv(shaderProgram, "normalMat", glm::value_ptr(glm::mat3(glm::transpose(glm::inverse(mv)))));
		rt3d::setLightPos(shaderProgram,glm::value_ptr(lightPos));

		//std::cout<<glewGetErrorString(glGetError())<<std::endl;
		GLuint uniform = glGetUniformLocation(shaderProgram,"eyePosition");
		glUniform3fv(uniform,1,glm::value_ptr(position));
		uniform = glGetUniformLocation(shaderProgram, "scaleBias");
		glUniform2fv(uniform,1,glm::value_ptr(scaleBias));

		rt3d::setEmissiveMaterial(shaderProgram,material1);

		rt3d::drawIndexedMesh(meshObjects[FLOOR],planeVertCount,GL_TRIANGLES);
		matStack.pop();
		matStack.push(glm::mat4(1.0));
		matStack.top() = glm::translate(matStack.top(),glm::vec3(2.0f,0.0f,1.0f));
		
		mv = viewMatrix * matStack.top();
		//set uniform
		rt3d::setLight(shaderProgram,light0);

		rt3d::setUniformMatrix4fv(shaderProgram,"projection",glm::value_ptr(projection));
		rt3d::setUniformMatrix4fv(shaderProgram,"view",glm::value_ptr(viewMatrix));
		rt3d::setUniformMatrix4fv(shaderProgram,"model",glm::value_ptr(matStack.top()));
		rt3d::setUniformMatrix3fv(shaderProgram, "normalMat", glm::value_ptr(glm::mat3(glm::transpose(glm::inverse(mv)))));
		rt3d::setLightPos(shaderProgram,glm::value_ptr(lightPos));

		//std::cout<<glewGetErrorString(glGetError())<<std::endl;
		uniform = glGetUniformLocation(shaderProgram,"eyePosition");
		glUniform3fv(uniform,1,glm::value_ptr(position));
		uniform = glGetUniformLocation(shaderProgram, "scaleBias");
		glUniform2fv(uniform,1,glm::value_ptr(scaleBias));
		
		rt3d::setEmissiveMaterial(shaderProgram,material1);

		rt3d::drawIndexedMesh(meshObjects[FLOOR],planeVertCount,GL_TRIANGLES);
		matStack.pop();
						matStack.push(glm::mat4(1.0));
		matStack.top() = glm::translate(matStack.top(),glm::vec3(-2.0f,0.0f,1.0f));
		
		mv = viewMatrix * matStack.top();
		//set uniform
		rt3d::setLight(shaderProgram,light0);

		rt3d::setUniformMatrix4fv(shaderProgram,"projection",glm::value_ptr(projection));
		rt3d::setUniformMatrix4fv(shaderProgram,"view",glm::value_ptr(viewMatrix));
		rt3d::setUniformMatrix4fv(shaderProgram,"model",glm::value_ptr(matStack.top()));
		rt3d::setUniformMatrix3fv(shaderProgram, "normalMat", glm::value_ptr(glm::mat3(glm::transpose(glm::inverse(mv)))));
		rt3d::setLightPos(shaderProgram,glm::value_ptr(lightPos));

		//std::cout<<glewGetErrorString(glGetError())<<std::endl;
		uniform = glGetUniformLocation(shaderProgram,"eyePosition");
		glUniform3fv(uniform,1,glm::value_ptr(position));
		uniform = glGetUniformLocation(shaderProgram, "scaleBias");
		glUniform2fv(uniform,1,glm::value_ptr(scaleBias));
		
		rt3d::setEmissiveMaterial(shaderProgram,material1);

		rt3d::drawIndexedMesh(meshObjects[FLOOR],planeVertCount,GL_TRIANGLES);
		matStack.pop();
						matStack.push(glm::mat4(1.0));
		matStack.top() = glm::translate(matStack.top(),glm::vec3(2.0f,0.0f,-1.0f));
		
		mv = viewMatrix * matStack.top();
		//set uniform
		rt3d::setLight(shaderProgram,light0);

		rt3d::setUniformMatrix4fv(shaderProgram,"projection",glm::value_ptr(projection));
		rt3d::setUniformMatrix4fv(shaderProgram,"view",glm::value_ptr(viewMatrix));
		rt3d::setUniformMatrix4fv(shaderProgram,"model",glm::value_ptr(matStack.top()));
		rt3d::setUniformMatrix3fv(shaderProgram, "normalMat", glm::value_ptr(glm::mat3(glm::transpose(glm::inverse(mv)))));
		rt3d::setLightPos(shaderProgram,glm::value_ptr(lightPos));

		//std::cout<<glewGetErrorString(glGetError())<<std::endl;
		uniform = glGetUniformLocation(shaderProgram,"eyePosition");
		glUniform3fv(uniform,1,glm::value_ptr(position));
		uniform = glGetUniformLocation(shaderProgram, "scaleBias");
		glUniform2fv(uniform,1,glm::value_ptr(scaleBias));
		
		rt3d::setEmissiveMaterial(shaderProgram,material1);

		rt3d::drawIndexedMesh(meshObjects[FLOOR],planeVertCount,GL_TRIANGLES);
		matStack.pop();
						matStack.push(glm::mat4(1.0));
		matStack.top() = glm::translate(matStack.top(),glm::vec3(0.0f,0.0f,-1.0f));
		
		mv = viewMatrix * matStack.top();
		//set uniform
		rt3d::setLight(shaderProgram,light0);

		rt3d::setUniformMatrix4fv(shaderProgram,"projection",glm::value_ptr(projection));
		rt3d::setUniformMatrix4fv(shaderProgram,"view",glm::value_ptr(viewMatrix));
		rt3d::setUniformMatrix4fv(shaderProgram,"model",glm::value_ptr(matStack.top()));
		rt3d::setUniformMatrix3fv(shaderProgram, "normalMat", glm::value_ptr(glm::mat3(glm::transpose(glm::inverse(mv)))));
		rt3d::setLightPos(shaderProgram,glm::value_ptr(lightPos));

		//std::cout<<glewGetErrorString(glGetError())<<std::endl;
		uniform = glGetUniformLocation(shaderProgram,"eyePosition");
		glUniform3fv(uniform,1,glm::value_ptr(position));
		uniform = glGetUniformLocation(shaderProgram, "scaleBias");
		glUniform2fv(uniform,1,glm::value_ptr(scaleBias));
		
		rt3d::setEmissiveMaterial(shaderProgram,material1);

		rt3d::drawIndexedMesh(meshObjects[FLOOR],planeVertCount,GL_TRIANGLES);
		matStack.pop();
						matStack.push(glm::mat4(1.0));
		matStack.top() = glm::translate(matStack.top(),glm::vec3(-2.0f,0.0f,-1.0f));
		
		mv = viewMatrix * matStack.top();
		//set uniform
		rt3d::setLight(shaderProgram,light0);

		rt3d::setUniformMatrix4fv(shaderProgram,"projection",glm::value_ptr(projection));
		rt3d::setUniformMatrix4fv(shaderProgram,"view",glm::value_ptr(viewMatrix));
		rt3d::setUniformMatrix4fv(shaderProgram,"model",glm::value_ptr(matStack.top()));
		rt3d::setUniformMatrix3fv(shaderProgram, "normalMat", glm::value_ptr(glm::mat3(glm::transpose(glm::inverse(mv)))));
		rt3d::setLightPos(shaderProgram,glm::value_ptr(lightPos));

		//std::cout<<glewGetErrorString(glGetError())<<std::endl;
		uniform = glGetUniformLocation(shaderProgram,"eyePosition");
		glUniform3fv(uniform,1,glm::value_ptr(position));
		uniform = glGetUniformLocation(shaderProgram, "scaleBias");
		glUniform2fv(uniform,1,glm::value_ptr(scaleBias));
		
		rt3d::setEmissiveMaterial(shaderProgram,material1);

		rt3d::drawIndexedMesh(meshObjects[FLOOR],planeVertCount,GL_TRIANGLES);
		matStack.pop();
}
void draw(SDL_Window * window) 
{

		glClearColor(0.1f,0.1f,0.1f,1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		drawWalls();
		drawFloors();
		//draw light
		glm::mat4 projection = glm::perspective(f,scrWidth/scrHeight,0.05f,120.0f);
		viewMatrix       = glm::lookAt(
		position,				// Camera is here
		position+directionD,	// and looks here : at the same position, plus "direction"
		up						// Head is up (set to 0,-1,0 to look upside-down)
		);
		glUseProgram(basicShader);
		glActiveTexture(GL_TEXTURE0);
		GLuint diffLoc = glGetUniformLocation(basicShader,"textureUnit0");
		glUniform1i(diffLoc,0);
		glBindTexture(GL_TEXTURE_2D,textures[10]);
		std::stack<glm::mat4> matStack;
		matStack.push(glm::mat4(1.0));
		matStack.top() = glm::translate(matStack.top(),glm::vec3(lightPos));
		matStack.top() = glm::scale(matStack.top(),glm::vec3(0.1f));
		rt3d::setUniformMatrix4fv(basicShader,"projection",glm::value_ptr(projection));
		rt3d::setUniformMatrix4fv(basicShader,"modelview",glm::value_ptr(viewMatrix*matStack.top()));
		glm::vec4 tmp = viewMatrix * lightPos;
		rt3d::setLightPos(basicShader,glm::value_ptr(tmp));
		rt3d::setEmissiveMaterial(basicShader,material2);
		rt3d::setUniformVector3f(basicShader,"eyePosition",glm::value_ptr(position));
		rt3d::setLight(basicShader,light0);
		rt3d::drawIndexedMesh(meshObjects[LIGHT],sphereVertCount,GL_TRIANGLES);
		SDL_GL_SwapWindow(window); // swap buffers
		timer = now;
}


// Program entry point - SDL manages the actual WinMain entry point for us
int main(int argc, char *argv[]) {
    SDL_Window * hWindow; // window handle
    SDL_GLContext glContext; // OpenGL context handle
    hWindow = setupRC(glContext); // Create window and render context 

	// Required on Windows *only* init GLEW to access OpenGL beyond 1.1
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) { // glewInit failed, something is seriously wrong
		std::cout << "glewInit failed, aborting." << endl;
		exit (1);
	}
	cout << glGetString(GL_VERSION) << endl;
	//initialize variables
	init();

	bool running = true; // set running to true
	SDL_Event sdlEvent;  // variable to detect SDL events
	while (running)	{	// the event loop
		while (SDL_PollEvent(&sdlEvent)) {
			if (sdlEvent.type == SDL_QUIT)
				running = false;
			else
			{
				running = handleEvents(sdlEvent);
			}
		}
		updateModel();
		draw(hWindow); // call the draw function
	}

    SDL_GL_DeleteContext(glContext);
    SDL_DestroyWindow(hWindow);
    SDL_Quit();
    return 0;
}