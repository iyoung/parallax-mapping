// parallax.vert


#version 330

// Some drivers require the following
//precision highp float;
//per vertex inputs stored on GPU memory
in vec3 in_Position;
in vec3 in_Normal;
in vec4 tangent;
//per mesh data sent at rendering time
uniform mat4 model; //object to world space transformations
uniform mat4 view;	//world to eye space transformations
uniform mat4 projection; //eye space to screen space
uniform mat3 normalMat; //for transforming normals in 

uniform vec4 lightPosition; //light position in world space
uniform vec3 eyePosition; //eye position in world space

//outputs to fragment shader

in vec2 in_TexCoord;

// multiply each vertex position by the MVP matrix
// and find V, L, and TBN Matrix vectors for the fragment shader
out VertexData
{
	vec3 ts_L; //view vector tangent space
	vec3 ts_V; //light vector tangent space
	vec3 ws_L; //world space light vector
	vec3 ws_V; //world space view vector
	vec3 ws_N; //world space surface normal
	vec2 ex_TexCoord;
	mat3 tbn;
	float dist;
}	vertex;

void main(void) 
{
	//transform to world space
	vec4 worldPos = model * vec4 (in_Position,1.0);
	vec3 worldNormal = normalize(model * vec4(in_Normal,0.0)).xyz;
	vec3 worldTangent = normalize(model * vec4(tangent.xyz,0.0)).xyz;

	//calculate vectors to light and camera
	vec3 worldToLight = lightPosition.xyz - worldPos.xyz;
	vertex.dist = length(worldToLight);
	worldToLight = normalize(worldToLight);
	vec3 worldToCam = normalize(eyePosition.xyz - worldPos.xyz);

	//calculate biTangent
	vec3 worldBiTangent = cross(worldNormal, worldTangent) * tangent.w;

	//transform light direction to tangent space
	vertex.ts_L = vec3(
						dot(worldToLight, worldTangent),
						dot(worldToLight, worldBiTangent),
						dot(worldToLight, worldNormal)
						);

	//transform cam direction to tangent space
	vertex.ts_V = vec3(
						dot(worldToCam, worldTangent),
						dot(worldToCam, worldBiTangent),
						dot(worldToCam, worldNormal)
						);

	//pass texture coordinates to fragment shader
	vertex.ex_TexCoord = in_TexCoord;
	vertex.ws_V = worldToCam;
	vertex.ws_L = worldToLight;
	vertex.ws_N = worldNormal;
	vertex.tbn = mat3 (worldTangent,worldBiTangent,worldNormal);
	gl_Position = projection * view * worldPos;

}